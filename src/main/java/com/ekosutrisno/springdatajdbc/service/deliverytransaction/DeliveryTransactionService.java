package com.ekosutrisno.springdatajdbc.service.deliverytransaction;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

/**
 * @author Eko Sutrisno
 * Jumat, 17/06/2022 15.11
 */

@Service
public class DeliveryTransactionService {
    private final JdbcTemplate jdbcTemplateDeliveryTransaction;

    public DeliveryTransactionService(@Qualifier("jdbcTemplateDeliveryTransaction") JdbcTemplate jdbcTemplateDeliveryTransaction) {
        this.jdbcTemplateDeliveryTransaction = jdbcTemplateDeliveryTransaction;
    }

    public boolean exist(String value) {
        String query = "SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END AS exist FROM delivery_header c WHERE delivery_no = ?";

        return Boolean.TRUE.equals(jdbcTemplateDeliveryTransaction
                .queryForObject(query, Boolean.class, value));
    }
}
