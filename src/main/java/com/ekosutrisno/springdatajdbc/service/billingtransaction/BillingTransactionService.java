package com.ekosutrisno.springdatajdbc.service.billingtransaction;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

/**
 * @author Eko Sutrisno
 * Selasa, 21/06/2022 21.55
 */
@Service
public class BillingTransactionService {
    private final JdbcTemplate jdbcTemplateBillingTransaction;

    public BillingTransactionService(@Qualifier("jdbcTemplateBillingTransaction") JdbcTemplate jdbcTemplateBillingTransaction) {
        this.jdbcTemplateBillingTransaction = jdbcTemplateBillingTransaction;
    }

    public boolean exist(String value) {
        String query = "SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END AS exist FROM billing_sales_header c WHERE billing_no = ?";

        return Boolean.TRUE.equals(jdbcTemplateBillingTransaction
                .queryForObject(query, Boolean.class, value));
    }

}
