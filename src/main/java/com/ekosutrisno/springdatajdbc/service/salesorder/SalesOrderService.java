package com.ekosutrisno.springdatajdbc.service.salesorder;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

/**
 * @author Eko Sutrisno
 * Jumat, 17/06/2022 15.11
 */

@Service
public class SalesOrderService {
    private final JdbcTemplate jdbcTemplateSalesOrder;

    public SalesOrderService(@Qualifier("jdbcTemplateSalesOrder") JdbcTemplate jdbcTemplateSalesOrder) {
        this.jdbcTemplateSalesOrder = jdbcTemplateSalesOrder;
    }

    public boolean exist(String value) {
        String query = "SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END AS exist FROM sales_order_header c WHERE order_number = ?";
        return Boolean.TRUE.equals(jdbcTemplateSalesOrder
                .queryForObject(query, Boolean.class, value));

    }
}
