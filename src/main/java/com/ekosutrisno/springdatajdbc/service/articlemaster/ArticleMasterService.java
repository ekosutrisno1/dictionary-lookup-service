package com.ekosutrisno.springdatajdbc.service.articlemaster;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

/**
 * @author Eko Sutrisno
 * Selasa, 21/06/2022 21.53
 */
@Service
public class ArticleMasterService {
    private final JdbcTemplate jdbcTemplateArticleMaster;

    public ArticleMasterService(@Qualifier("jdbcTemplateArticleMaster") JdbcTemplate jdbcTemplateArticleMaster) {
        this.jdbcTemplateArticleMaster = jdbcTemplateArticleMaster;
    }

    public boolean exist(String value) {
        String query = "SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END AS exist FROM article_basic_data c WHERE article_code = ?";

        return Boolean.TRUE.equals(jdbcTemplateArticleMaster
                .queryForObject(query, Boolean.class, value));
    }
}
