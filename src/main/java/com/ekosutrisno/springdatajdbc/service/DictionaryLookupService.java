package com.ekosutrisno.springdatajdbc.service;

import com.ekosutrisno.springdatajdbc.model.GeneralResponse;
import com.ekosutrisno.springdatajdbc.model.ResponseMetadata;

/**
 * @author Eko Sutrisno
 * Jumat, 17/06/2022 16.51
 */
public interface DictionaryLookupService {
    GeneralResponse<ResponseMetadata> lookupCheck(String stringValue);
}
