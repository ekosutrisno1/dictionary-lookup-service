package com.ekosutrisno.springdatajdbc.service;

import com.ekosutrisno.springdatajdbc.config.EnvironmentConfig;
import com.ekosutrisno.springdatajdbc.model.*;
import com.ekosutrisno.springdatajdbc.service.articlemaster.ArticleMasterService;
import com.ekosutrisno.springdatajdbc.service.billingtransaction.BillingTransactionService;
import com.ekosutrisno.springdatajdbc.service.deliverytransaction.DeliveryTransactionService;
import com.ekosutrisno.springdatajdbc.service.salesorder.SalesOrderService;
import org.springframework.stereotype.Service;

import java.util.Date;

import static org.springframework.http.HttpStatus.OK;

/**
 * @author Eko Sutrisno
 * Jumat, 17/06/2022 17.00
 */
@Service
public class DictionaryLookupServiceImpl implements DictionaryLookupService {

    private final DeliveryTransactionService deliveryTransactionService;
    private final SalesOrderService salesOrderService;
    private final ArticleMasterService articleMasterService;
    private final BillingTransactionService billingTransactionService;
    private final EnvironmentConfig env;

    public DictionaryLookupServiceImpl(DeliveryTransactionService deliveryTransactionService, SalesOrderService salesOrderService, ArticleMasterService articleMasterService, BillingTransactionService billingTransactionService, EnvironmentConfig env) {
        this.deliveryTransactionService = deliveryTransactionService;
        this.salesOrderService = salesOrderService;
        this.articleMasterService = articleMasterService;
        this.billingTransactionService = billingTransactionService;
        this.env = env;
    }

    @Override
    public GeneralResponse<ResponseMetadata> lookupCheck(String stringValue) {
        /*
         * Info for Response Char Code
         * A: Article Code,
         * B: Order Number,
         * C: Delivery Number,
         * D: Invoice Number
         * N: Not Recognize
         */
        Result result = Result.N;
        Value value = Value.NOT_RECOGNIZE;

        /*
         * Please update or add checks here if needed.
         */
        if (articleMasterService.exist(stringValue)) {
            result = Result.A;
            value = Value.ARTICLE_CODE;
        } else if (salesOrderService.exist(stringValue)) {
            result = Result.B;
            value = Value.ORDER_NUMBER;
        } else if (deliveryTransactionService.exist(stringValue)) {
            result = Result.C;
            value = Value.DELIVERY_NUMBER;
        } else if (billingTransactionService.exist(stringValue)) {
            result = Result.D;
            value = Value.INVOICE_NUMBER;
        }

        return GeneralResponse.<ResponseMetadata>builder()
                .apiVersion(env.getApiVersion())
                .organization(env.getOrganization())
                .info(Info.builder().statusCode(OK.value()).statusMessage(OK.toString()).build())
                .requestMetadata(RequestMetadata.builder().requestValue(stringValue).build())
                .responseMetadata(
                        ResponseMetadata
                                .builder()
                                .result(result)
                                .resultValue(value)
                                .timestamp(new Date()).build()
                ).build();
    }
}
