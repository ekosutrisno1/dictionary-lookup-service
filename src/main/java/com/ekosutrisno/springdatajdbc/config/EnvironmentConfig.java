package com.ekosutrisno.springdatajdbc.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author Eko Sutrisno
 * Sabtu, 18/06/2022 08.42
 */
@Configuration
@ConfigurationProperties(prefix = "app.metadata")
@Setter
@Getter
public class EnvironmentConfig {
    private String apiVersion;
    private String organization;
}
