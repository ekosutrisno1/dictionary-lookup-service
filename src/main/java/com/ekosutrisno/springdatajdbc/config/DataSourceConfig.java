package com.ekosutrisno.springdatajdbc.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * @author Eko Sutrisno
 * Jumat, 17/06/2022 14.54
 */
@Configuration
public class DataSourceConfig {

    @Bean(name = "hikariDataSourceDeliveryTransaction")
    @Primary
    @ConfigurationProperties("app.datasource.delivery-transaction")
    public HikariDataSource hikariDataSourceDeliveryTransaction() {
        return DataSourceBuilder
                .create()
                .type(HikariDataSource.class)
                .build();
    }

    @Bean(name = "jdbcTemplateDeliveryTransaction")
    @Primary
    public JdbcTemplate jdbcTemplateDeliveryTransaction(@Qualifier("hikariDataSourceDeliveryTransaction") HikariDataSource hikariDataSourceDeliveryTransaction) {
        return new JdbcTemplate(hikariDataSourceDeliveryTransaction);
    }

    @Bean(name = "hikariDataSourceSalesOrder")
    @ConfigurationProperties("app.datasource.sales-order")
    public HikariDataSource hikariDataSourceSalesOrder() {
        return DataSourceBuilder
                .create()
                .type(HikariDataSource.class)
                .build();
    }

    @Bean(name = "jdbcTemplateSalesOrder")
    public JdbcTemplate jdbcTemplateSalesOrder(@Qualifier("hikariDataSourceSalesOrder") HikariDataSource hikariDataSourceSalesOrder) {
        return new JdbcTemplate(hikariDataSourceSalesOrder);
    }

    @Bean(name = "hikariDataSourceArticleMaster")
    @ConfigurationProperties("app.datasource.article-master")
    public HikariDataSource hikariDataSourceArticleMaster() {
        return DataSourceBuilder
                .create()
                .type(HikariDataSource.class)
                .build();
    }

    @Bean(name = "jdbcTemplateArticleMaster")
    public JdbcTemplate jdbcTemplateArticleMaster(@Qualifier("hikariDataSourceArticleMaster") HikariDataSource hikariDataSourceArticleMaster) {
        return new JdbcTemplate(hikariDataSourceArticleMaster);
    }

    @Bean(name = "hikariDataSourceBillingTransaction")
    @ConfigurationProperties("app.datasource.billing-transaction")
    public HikariDataSource hikariDataSourceBillingTransaction() {
        return DataSourceBuilder
                .create()
                .type(HikariDataSource.class)
                .build();
    }

    @Bean(name = "jdbcTemplateBillingTransaction")
    public JdbcTemplate jdbcTemplateBillingTransaction(@Qualifier("hikariDataSourceBillingTransaction") HikariDataSource hikariDataSourceBillingTransaction) {
        return new JdbcTemplate(hikariDataSourceBillingTransaction);
    }
}
