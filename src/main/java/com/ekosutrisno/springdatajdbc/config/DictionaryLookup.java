package com.ekosutrisno.springdatajdbc.config;

import com.ekosutrisno.springdatajdbc.model.ResponseMetadata;
import com.ekosutrisno.springdatajdbc.model.Result;
import com.ekosutrisno.springdatajdbc.model.Value;

import java.util.Date;

/**
 * @author Eko Sutrisno
 * Selasa, 21/06/2022 14.43
 */
public abstract class DictionaryLookup {
    public DictionaryLookup nextLookup;

    public DictionaryLookup linkWith(DictionaryLookup nextLookup) {
        this.nextLookup = nextLookup;
        return nextLookup;
    }

    public abstract ResponseMetadata isExist(String numericValue);

    protected ResponseMetadata checkNext(String numericValue) {
        return nextLookup.isExist(numericValue);
    }
}
