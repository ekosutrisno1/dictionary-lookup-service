package com.ekosutrisno.springdatajdbc.controller;

import com.ekosutrisno.springdatajdbc.model.GeneralResponse;
import com.ekosutrisno.springdatajdbc.model.ResponseMetadata;
import com.ekosutrisno.springdatajdbc.service.DictionaryLookupService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Eko Sutrisno
 * Jumat, 17/06/2022 16.33
 */
@RestController
@RequestMapping(path = "/dictionary/lookup", produces = MediaType.APPLICATION_JSON_VALUE)
public class DictionaryControllerLookup {

    private final DictionaryLookupService dictionaryLookupService;

    public DictionaryControllerLookup(DictionaryLookupService dictionaryLookupService) {
        this.dictionaryLookupService = dictionaryLookupService;
    }

    @GetMapping(path = "/{stringValue}")
    public GeneralResponse<ResponseMetadata> lookup(@PathVariable("stringValue") String stringValue) {
        return dictionaryLookupService.lookupCheck(stringValue);
    }
}
