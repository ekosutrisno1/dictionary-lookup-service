package com.ekosutrisno.springdatajdbc.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Eko Sutrisno
 * Sabtu, 18/06/2022 08.54
 */
@Builder
@Setter
@Getter
public class Info {
    private int statusCode;
    private String statusMessage;
}
