package com.ekosutrisno.springdatajdbc.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Eko Sutrisno
 * Jumat, 17/06/2022 16.54
 */
@Builder
@Getter
@Setter
public class RequestMetadata {
    private String requestValue;
}
