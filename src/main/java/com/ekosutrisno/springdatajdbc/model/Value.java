package com.ekosutrisno.springdatajdbc.model;

/**
 * @author Eko Sutrisno
 * Jumat, 17/06/2022 17.23
 */
public enum Value {
    ARTICLE_CODE,
    ORDER_NUMBER,
    DELIVERY_NUMBER,
    INVOICE_NUMBER,
    NOT_RECOGNIZE
}
