package com.ekosutrisno.springdatajdbc.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Eko Sutrisno
 * Jumat, 17/06/2022 16.52
 */
@Builder
@Getter
@Setter
public class GeneralResponse<T> {
    private String apiVersion;
    private String organization;
    private Info info;
    private RequestMetadata requestMetadata;
    private T responseMetadata;
}
