package com.ekosutrisno.springdatajdbc.model;

import lombok.*;

import java.util.Date;

/**
 * @author Eko Sutrisno
 * Jumat, 17/06/2022 16.58
 */
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ResponseMetadata {
    private Result result;
    private Value resultValue;
    private Date timestamp;
}
