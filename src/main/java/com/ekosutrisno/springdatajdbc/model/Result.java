package com.ekosutrisno.springdatajdbc.model;

/**
 * @author Eko Sutrisno
 * Jumat, 17/06/2022 17.18
 */
public enum Result {
    A, B, C, D, N
}
