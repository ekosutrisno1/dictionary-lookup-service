package com.ekosutrisno.springdatajdbc.service.salesorder;

import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author Eko Sutrisno
 * Jumat, 17/06/2022 16.10
 */
@SpringBootTest
@Slf4j
class SalesOrderServiceImplTest {
    @Autowired
    SalesOrderService salesOrderService;

    @Test
    void existByOrderNumber() {
        String orderNumber = "1100009069";

        long start = System.currentTimeMillis();

        boolean isExist = salesOrderService.exist(orderNumber);

        log.info("Duration {} ms", (System.currentTimeMillis() - start));

        Assertions.assertThat(isExist).isTrue();
    }
}