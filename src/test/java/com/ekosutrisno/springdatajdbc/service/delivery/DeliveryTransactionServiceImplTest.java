package com.ekosutrisno.springdatajdbc.service.delivery;

import com.ekosutrisno.springdatajdbc.service.deliverytransaction.DeliveryTransactionService;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author Eko Sutrisno
 * Jumat, 17/06/2022 15.23
 */
@SpringBootTest
@Slf4j
class DeliveryTransactionServiceImplTest {

    @Autowired
    DeliveryTransactionService deliveryTransactionService;

    @Test
    void existByDeliveryNo() {
        String deliveryNo = "0330000904";

        long start = System.currentTimeMillis();

        boolean isExist = deliveryTransactionService.exist(deliveryNo);

        log.info("Duration {} ms", (System.currentTimeMillis() - start));

        Assertions.assertThat(isExist).isTrue();
    }
}