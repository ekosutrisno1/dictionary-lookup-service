package com.ekosutrisno.springdatajdbc.service;

import com.ekosutrisno.springdatajdbc.model.GeneralResponse;
import com.ekosutrisno.springdatajdbc.model.ResponseMetadata;
import com.ekosutrisno.springdatajdbc.model.Result;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Eko Sutrisno
 * Sabtu, 18/06/2022 09.28
 */
@SpringBootTest
@Slf4j
class DictionaryLookupServiceImplTest {
    @Autowired
    DictionaryLookupService dictionaryLookupService;

    @Test
    @DisplayName("Result with Not Recognize")
    void lookupCheckAndReturnNotRecognize() {
        String stringValue = "GAK-ADA";

        GeneralResponse<ResponseMetadata> result = dictionaryLookupService.lookupCheck(stringValue);

        log.info("Result is: {} => {}", result.getResponseMetadata().getResult().toString(), result.getResponseMetadata().getResultValue().toString());

        Assertions.assertThat(result.getResponseMetadata().getResult()).isEqualTo(Result.N);
    }

    @Test
    @DisplayName("Result with Order Number")
    void lookupCheckAndReturnOrderNumber() {
        String stringValue = "1100009069";

        GeneralResponse<ResponseMetadata> result = dictionaryLookupService.lookupCheck(stringValue);

        log.info("Result is: {} => {}", result.getResponseMetadata().getResult().toString(), result.getResponseMetadata().getResultValue().toString());

        Assertions.assertThat(result.getResponseMetadata().getResult()).isEqualTo(Result.B);
    }

    @Test
    @DisplayName("Result with Delivery Number")
    void lookupCheckAndReturnDeliveryNumber() {
        String stringValue = "0330000904";

        GeneralResponse<ResponseMetadata> result = dictionaryLookupService.lookupCheck(stringValue);

        log.info("Result is: {} => {}", result.getResponseMetadata().getResult().toString(), result.getResponseMetadata().getResultValue().toString());

        Assertions.assertThat(result.getResponseMetadata().getResult()).isEqualTo(Result.C);
    }
}