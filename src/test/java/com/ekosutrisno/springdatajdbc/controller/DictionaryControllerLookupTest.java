package com.ekosutrisno.springdatajdbc.controller;

import com.ekosutrisno.springdatajdbc.model.GeneralResponse;
import com.ekosutrisno.springdatajdbc.model.ResponseMetadata;
import com.ekosutrisno.springdatajdbc.model.Result;
import com.ekosutrisno.springdatajdbc.service.DictionaryLookupService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.JsonPathResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author Eko Sutrisno
 * Sabtu, 18/06/2022 09.45
 */
@SpringBootTest
@AutoConfigureMockMvc
class DictionaryControllerLookupTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    @DisplayName("Integration Testing Controller")
    void lookup() throws Exception {
        // Given
        String stringValue = "1100009069";

        // When
        ResultActions resultActions = mockMvc
                .perform(get("/dictionary/lookup/" + stringValue)
                        .contentType(MediaType.APPLICATION_JSON));

        // Then
        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseMetadata.result").value(Result.B.toString()))
                .andDo(print())
                .andReturn();
    }
}