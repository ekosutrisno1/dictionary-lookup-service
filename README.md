# Dictionary Lookup Service

This service aims to check whether a frequent is an Article Code / Order Number / Delivery No / Invoice No by checking
Exists or not in their respective databases.

## Info for Response Char Code

```shell
A: Article Code      | ARTICLE_CODE
B: Order Number      | ORDER_NUMBER
C: Delivery Number   | DELIVERY_NUMBER
D: Invoice Number    | INVOICE_NUMBER
N: Not Recognize     | NOT_RECOGNIZE
```

## API Spec

- Dictionary Lookup Endpoint
    - Method `GET`
    - URI Path `{baseUrl}/dictionary/lookup/${stringValue}`
    - Path Variable `stringValue: String`
    - Response Type `application/json`
    - Response Body `(200: Sucess)`:
      ````json
      {
          "apiVersion": "v0.0.1-Beta",
          "organization": "PT Azec Indonesia Management Service",
          "info": {
            "statusCode": 200,
            "statusMessage": "200 OK"
          },
          "requestMetadata": {
            "requestValue": "0330000904"
          },
          "responseMetadata": {
            "result": "C",
            "resultValue": "DELIVERY_NUMBER",
            "timestamp": "2022-06-18T01:24:45.469+00:00"
          }
      }
      ````